<section class="main-info bg-grad" id="main-info">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-5 col-xl-6 img-col d-flex aling-items-center">
                <div class="img-container d-flex justify-content-center">
                    <img src="assets/img/apoyo.png" alt="">
                </div>
            </div>
            <div class="col-12 col-md-7 col-xl-6 txt-col">
            <form id="formLogin"  name="formLogin" >
                <div class="alert alert-danger" id="alertMessage"   role="alert">
                    
                </div>
                <div class="form-group">
                    <label for="user">Usuario</label>
                    <input type="text" required="true" class="form-control" id="user" >
                </div>
                <div class="form-group">
                    <label for="password">Clave</label>
                    <input type="password" required="true" class="form-control" id="password">
                </div>
                <button type="submit"  class="btn btn-primary">Iniciar Sesión</button>
                </form>
            </div>
        </div>
    </div>
</section>

<script>
    $("#alertMessage").hide();
    $("form#formLogin").submit(function (e){
        e.preventDefault();
        var user = $("#user").val();
        var password = $("#password").val();
        $.ajax({
            // la URL para la petición
            url : 'controller/UserController.php',

            // la información a enviar
            // (también es posible utilizar una cadena de datos)
            
            data : JSON.stringify({ userName : user , password: password }),

            // especifica si será una petición POST o GET
            type : 'POST',

            // el tipo de información que se espera de respuesta
            dataType : 'json',

            // código a ejecutar si la petición es satisfactoria;
            // la respuesta es pasada como argumento a la función
            success : function(json) {
                if(json.status == "ok"){
                    localStorage['userToken'] = json.result.token;
                    localStorage['role'] = json.result.role;
                    localStorage['idUser'] = json.result.idUser;
                    window.location="main.php";
                }else{
                    $("#alertMessage").show();
                    
                    $('#alertMessage').text(json.result.error_msg);
                }
                
            },

            // código a ejecutar si la petición falla;
            // son pasados como argumentos a la función
            // el objeto de la petición en crudo y código de estatus de la petición
            error : function(xhr, status) {
                alert('Disculpe, existió un problema ' + status);
            },

            // código a ejecutar sin importar si la petición falló o no
            /*complete : function(xhr, status) {
                alert('Petición realizada ' + localStorage['userToken'] || 'defaultValue');
            }*/
        });
    });



</script>