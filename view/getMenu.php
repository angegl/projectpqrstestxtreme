<header class="header-area fixed-top">
    <div class="main-menu">
        <nav class="navbar" >
            <div class="container border-bottom border-info rounded">
                <ul class="nav ml-auto mr-auto mr-md-0" id="menu">
                    
                </ul>
            </div>
        </nav>
    </div>
</header>
<section class='main-info bg-grad' id='main-info'>
    <div class='container'>
        <div class='row'>
            <div class='col-12 col-md-12 col-xl-12  d-flex aling-items-center'>
                <table class='table'>
                    <thead id="tHead">

                    </thead>
                    <tbody id="tBody">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Agregar Pqrs</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Tipo</label>
                        <select class="form-control" id="typePqr">
                            <option value="P">Peticion</option>
                            <option value="Q">Queja</option>
                            <option value="R">Reclamo</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Asunto</label>
                        <textarea class="form-control" id="affair" rows="3"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="save" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="staticBackdropUser" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Agregar Usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Usuario</label>
                        <input type="text" class="form-control" id="userName" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="password">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Role</label>
                        <select class="form-control" id="role">
                            <option value="A">Administrador</option>
                            <option value="U">Usuario</option>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="saveUser" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script>
if(localStorage['userToken']){
    if(localStorage['role']==="A"){
        $("#menu").html("<li class='nav-item ' ><a href='#' id='pqrs' class='nav-link'>PQRS</a></li><li class='nav-item'><a href='#' id='usersList' class='nav-link'>Usuarios</a></li> <li class='nav-item'><a href='#' id='closeSesion' class='nav-link'>Cerrar Sesion</a></li>");

    }else{
        $("#menu").html("<li class='nav-item ' ><a href='#' id='pqrs' class='nav-link'>Mis PQRS</a></li><li class='nav-item'><a href='#' id='closeSesion' class='nav-link'>Cerrar Sesion</a></li>");
    }

    $(document).ready(function () {
        loadPqrs();
    });

}

$("#pqrs").click(function () {
    loadPqrs();
});

$("#usersList").click(function () {
    loadUsers();
});

$("#save").click(function () {
    $.ajax({
        // la URL para la petición
        url : 'controller/PqrController.php',

        // la información a enviar
        // (también es posible utilizar una cadena de datos)

        data : JSON.stringify({
            token : localStorage['userToken'] ,
            idUser :  localStorage['idUser'],
            typePqr : $("#typePqr").val(),
            affair : $("#affair").val()

        }),

        // especifica si será una petición POST o GET
        type : 'POST',

        // el tipo de información que se espera de respuesta
        dataType : 'json',
        headers: {'Authorization': 'Bearer '+localStorage['userToken']},

        // código a ejecutar si la petición es satisfactoria;
        // la respuesta es pasada como argumento a la función
        success : function(json) {
            console.log(json);
            if(json.status == "ok"){
                loadPqrs();
                $("#affair").val("");
                $('#staticBackdrop').modal('hide');
            }else{
                alert("Error al almacenar los datos");
            }

        },

        // código a ejecutar si la petición falla;
        // son pasados como argumentos a la función
        // el objeto de la petición en crudo y código de estatus de la petición
        error : function(xhr, status) {
            alert('Disculpe, existió un problema ' + status);
        },

        // código a ejecutar sin importar si la petición falló o no
        /*complete : function(xhr, status) {
            alert('Petición realizada ' + localStorage['userToken'] || 'defaultValue');
        }*/
    });
});


$("#saveUser").click(function () {
    $.ajax({
        // la URL para la petición
        url : 'controller/UserController.php',

        // la información a enviar
        // (también es posible utilizar una cadena de datos)

        data : JSON.stringify({
            method : "create" ,
            userName :  $("#userName").val(),
            password : $("#password").val(),
            role : $("#role").val()

        }),

        // especifica si será una petición POST o GET
        type : 'POST',

        // el tipo de información que se espera de respuesta
        dataType : 'json',
        headers: {'Authorization': 'Bearer '+localStorage['userToken']},

        // código a ejecutar si la petición es satisfactoria;
        // la respuesta es pasada como argumento a la función
        success : function(json) {
            console.log(json);
            if(json.status == "ok"){
                loadUsers();
                $("#userName").val("");
                $('#password').val("");
                $('#staticBackdropUser').modal('hide');
            }else{
                alert("Error al almacenar los datos");
            }

        },

        // código a ejecutar si la petición falla;
        // son pasados como argumentos a la función
        // el objeto de la petición en crudo y código de estatus de la petición
        error : function(xhr, status) {
            alert('Disculpe, existió un problema ' + status);
        },

        // código a ejecutar sin importar si la petición falló o no
        /*complete : function(xhr, status) {
            alert('Petición realizada ' + localStorage['userToken'] || 'defaultValue');
        }*/
    });
});


function loadPqrs() {
    var iconAdd = "<i data-toggle='modal' data-target='#staticBackdrop' class='fas fa-plus'></i>"
    if(localStorage['role']== "A") iconAdd="";
    $("#tHead").html("<tr>\n" +
            "<th scope='col'>#</th>" +
            "<th scope='col'>Usuario</th>" +
            "<th scope='col'>Asunto</th>" +
            "<th scope='col'>Estado</th>" +
            "<th scope='col'>Tipo</th>" +
            "<th scope='col' > "+ iconAdd +" </th>" +
            "/tr>");
    $.ajax({
        // la URL para la petición
        url : 'controller/PqrController.php?idUser='+localStorage['idUser']+"&role="+localStorage['role'],

        // la información a enviar
        // (también es posible utilizar una cadena de datos)

        data : JSON.stringify({
            token : localStorage['userToken']
        }),

        // especifica si será una petición POST o GET
        type : 'GET',

        // el tipo de información que se espera de respuesta
        dataType : 'json',
        headers: {'Authorization': 'Bearer '+localStorage['userToken']},

        // código a ejecutar si la petición es satisfactoria;
        // la respuesta es pasada como argumento a la función
        success : function(json) {

            if(json.status == "ok"){
                var pqrsArray= json.data;
                var rows = "";
                counter = 1;
                console.log(pqrsArray);
                for (i = 0; i < pqrsArray.length; i++ ){
                    status = pqrsArray[i].status;
                    switch (status) {
                        case "N": status="Nuevo";
                        break;
                        case "E": status="En Ejecucion";
                        break;
                        case "C": status="Cerrado";
                        break;
                        default: status="Desconocido";
                        break;
                    }
                    typePqr = pqrsArray[i].typePqr;
                    switch (typePqr) {
                        case "P": typePqr="Peticion";
                            break;
                        case "Q": typePqr="Queja";
                            break;
                        case "R": typePqr="Reclamo";
                            break;
                        default: typePqr="Desconocido";
                            break;
                    }
                    id = pqrsArray[i].id;
                    rows += "<tr>" +
                        "    <td scope='row'> "+ counter +" </td>" +
                        "    <td>"+ pqrsArray[i].userName +"</td>" +
                        "    <td>"+  pqrsArray[i].affair  +"</td>" +
                        "    <td>"+  status +"</td>" +
                        "    <td>"+  typePqr +"</td>" +
                        "    <td> <i id='delete' data-id='"+ id +"' class='delete fas fa-trash-alt'></i> </td>" +
                        "</tr>";

                    counter++;
                }
                $("#tBody").html(rows);
                deletePqr();
            }else{

            }

        },

        // código a ejecutar si la petición falla;
        // son pasados como argumentos a la función
        // el objeto de la petición en crudo y código de estatus de la petición
        error : function(xhr, status) {
            alert('Disculpe, existió un problema ' + status);
        },

        // código a ejecutar sin importar si la petición falló o no
        /*complete : function(xhr, status) {
            alert('Petición realizada ' + localStorage['userToken'] || 'defaultValue');
        }*/
    });
}

$("#closeSesion").click(function () {
    window.location="index.php";
});

function loadUsers() {
    $("#tHead").html("<tr>\n" +
        "                        <th scope='col'>#</th>\n" +
        "                        <th scope='col'>Nombre</th>\n" +
        "                        <th scope='col'>Rol</th>\n" +
        "                        <th scope='col'> <i data-toggle=\"modal\" data-target=\"#staticBackdropUser\" class=\"fas fa-plus\"></i></th>\n" +
        "                    </tr>");
    $.ajax({
        // la URL para la petición
        url : 'controller/UserController.php',

        // la información a enviar
        // (también es posible utilizar una cadena de datos)

        data : JSON.stringify({ token : localStorage['userToken'] }),

        // especifica si será una petición POST o GET
        type : 'GET',

        // el tipo de información que se espera de respuesta
        dataType : 'json',
        headers: {'Authorization': 'Bearer '+localStorage['userToken']},

        // código a ejecutar si la petición es satisfactoria;
        // la respuesta es pasada como argumento a la función
        success : function(json) {

            if(json.status == "ok"){
                var usersArray= json.data;
                var rows = "";
                counter = 1;
                console.log(usersArray);
                for (i = 0; i < usersArray.length; i++ ){
                    role = usersArray[i].role;
                    if(role == "A") role ="Administrador";
                    else role ="Usuario";
                    id = usersArray[i].id;
                    rows += "<tr>" +
                        "    <td scope='row'> "+ counter +" </td>" +
                        "    <td>"+ usersArray[i].userName +"</td>" +
                        "    <td>"+  role  +"</td>" +
                        "    <td> <i id='delete' data-id='"+ id +"' class='deleteUser fas fa-trash-alt'></i> </td>" +
                        "</tr>";

                    counter++;
                }
                $("#tBody").html(rows);
                deleteUser();
            }else{

            }

        },

        // código a ejecutar si la petición falla;
        // son pasados como argumentos a la función
        // el objeto de la petición en crudo y código de estatus de la petición
        error : function(xhr, status) {
            alert('Disculpe, existió un problema ' + status);
        },

        // código a ejecutar sin importar si la petición falló o no
        /*complete : function(xhr, status) {
            alert('Petición realizada ' + localStorage['userToken'] || 'defaultValue');
        }*/
    });
}

function deletePqr() {
    $(".delete").click(function () {
        var confirmWindow = confirm("¿Esta seguro que desea eliminar el registro?");
        if(confirmWindow){
            $.ajax({
                // la URL para la petición
                url : 'controller/PqrController.php',

                // la información a enviar
                // (también es posible utilizar una cadena de datos)


                data : JSON.stringify({
                    id : $(this).attr("data-id") ,

                }),

                // especifica si será una petición POST o GET
                type : 'DELETE',

                // el tipo de información que se espera de respuesta
                dataType : 'json',
                headers: {'Authorization': 'Bearer '+localStorage['userToken']},

                // código a ejecutar si la petición es satisfactoria;
                // la respuesta es pasada como argumento a la función
                success : function(json) {
                    console.log(json);
                    if(json.status == "ok"){
                        alert("Datos eliminados Correctamente");
                        loadPqrs();
                    }else{

                    }

                },

                // código a ejecutar si la petición falla;
                // son pasados como argumentos a la función
                // el objeto de la petición en crudo y código de estatus de la petición
                error : function(xhr, status) {
                    alert('Disculpe, existió un problema ' + status);
                },

                // código a ejecutar sin importar si la petición falló o no
                /*complete : function(xhr, status) {
                    alert('Petición realizada ' + localStorage['userToken'] || 'defaultValue');
                }*/
            });
        }
    });
}

function deleteUser() {
    $(".deleteUser").click(function () {
        var confirmWindow = confirm("¿Esta seguro que desea eliminar el registro?");
        if(confirmWindow){
            $.ajax({
                // la URL para la petición
                url : 'controller/UserController.php',

                // la información a enviar
                // (también es posible utilizar una cadena de datos)


                data : JSON.stringify({
                    id : $(this).attr("data-id") ,

                }),

                // especifica si será una petición POST o GET
                type : 'DELETE',

                // el tipo de información que se espera de respuesta
                dataType : 'json',
                headers: {'Authorization': 'Bearer '+localStorage['userToken']},

                // código a ejecutar si la petición es satisfactoria;
                // la respuesta es pasada como argumento a la función
                success : function(json) {
                    console.log(json);
                    if(json.status == "ok"){
                        alert("Datos eliminados Correctamente");
                        loadUsers();
                    }else{
                        alert("Error al eliminar los datos");
                    }

                },

                // código a ejecutar si la petición falla;
                // son pasados como argumentos a la función
                // el objeto de la petición en crudo y código de estatus de la petición
                error : function(xhr, status) {
                    alert('Disculpe, existió un problema ' + status);
                },

                // código a ejecutar sin importar si la petición falló o no
                /*complete : function(xhr, status) {
                    alert('Petición realizada ' + localStorage['userToken'] || 'defaultValue');
                }*/
            });
        }
    });
}
</script>
