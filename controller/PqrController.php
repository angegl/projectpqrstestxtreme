<?php
require_once("../model/Responses.php");
require_once("../dataBase/Connector.php");
require_once("../model/UserToken.php");
require_once("../model/Pqr.php");

class PqrController
{
    var $pqrController;

    function __construct()
    {
        $this->pqrController = Pqr::getListInObjects(null);
    }

    public function index(){
        $responses = new Responses;
        $rowset = $this->pqrController;
        header("Content-Type: application/json");
        if(isset($rowset[0]->id)) {
            $result = $responses->response;

            $result["result"] = array(
                "data" => $rowset
            );

            echo json_encode($result);
        }else {
            $response = array (
                "status" => "Error",
                "Message" => "No se encontraron datos"
            );
            echo json_encode($response);
        }

    }

}

if ($_SERVER['REQUEST_METHOD'] == "GET"){

    $headers = apache_request_headers();
    if (isset($headers['Authorization'])){
        // listar
        $arrays = file_get_contents("php://input");

        //enviamos los datos al manejador
        $idUser = $_GET['idUser'];
        $role = $_GET['role'];
        $filter = null;
        if($role=='U') $filter = " idUser = '$idUser'";
        $response = array (
            "status" => "ok",
            "data" => Pqr::getListInObjects($filter)
        );
    }else{
        $response = array (
            "status" => "Error",
            "Message" => "No tienes acesso"
        );
    }

    echo json_encode($response);
}elseif($_SERVER['REQUEST_METHOD'] == "POST"){

    $headers = apache_request_headers();
    if (isset($headers['Authorization'])){
        //recibir datos
        $postBody = file_get_contents("php://input");

        //enviamos los datos al manejador
        $postBody = json_decode($postBody);
        $idUser= $postBody->idUser;
        $typePqr= $postBody->typePqr;
        $affair = $postBody->affair;
        $status = "N";
        $dataArray = Pqr::create($idUser, $typePqr, $affair, $status);

        //delvolvemos una respuesta
        header('Content-Type: application/json');
        if(isset($dataArray["result"]["error_id"])){
            $responseCode = $dataArray["result"]["error_id"];
            http_response_code($responseCode);
        }else{
            http_response_code(200);
        }
        echo json_encode($dataArray);
    }else{
        $response = array (
            "status" => "Error",
            "Message" => "No tienes acesso"
        );
    }

}elseif ($_SERVER['REQUEST_METHOD'] == "DELETE"){


    $headers = apache_request_headers();
    if (isset($headers['Authorization'])){
        $postBody = file_get_contents("php://input");
        $postBody = json_decode($postBody);
        $dataArray = Pqr::delete($postBody->id);
        header('Content-Type: application/json');
        if(isset($dataArray["result"]["error_id"])){
            $responseCode = $dataArray["result"]["error_id"];
            http_response_code($responseCode);
        }else{
            http_response_code(200);
        }
        echo json_encode($dataArray);
    }else{
    $response = array (
        "status" => "Error",
        "Message" => "No tienes acesso"
    );
}
}
?>