<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author USER
 */

require_once("../model/Responses.php");
require_once("../dataBase/Connector.php");
require_once("../model/User.php");
require_once("../model/UserToken.php");

class UserController {
    //put your code here
    
    var $users;
    
    function __construct()
    {
        $data = User::getListInObjects(null);
        $this->users = $data;
    }

    

}
if ($_SERVER['REQUEST_METHOD'] == "GET"){

    $headers = apache_request_headers();
    if (isset($headers['Authorization'])){
        // listar
        $response = array (
            "status" => "ok",
            "data" => User::getListInObjects(null)
        );
    }else{
        $response = array (
            "status" => "Error",
            "Message" => "No tienes acesso"
        );
    }

    echo json_encode($response);
}elseif($_SERVER['REQUEST_METHOD'] == "POST"){

    //recibir datos
    $postBody = file_get_contents("php://input");

    $array = json_decode($postBody);

    if(!isset($array->method)) {
        //enviamos los datos al manejador
        $dataArray = User::login($postBody);

        //delvolvemos una respuesta
        header('Content-Type: application/json');
        if (isset($dataArray["result"]["error_id"])) {
            $responseCode = $dataArray["result"]["error_id"];
            http_response_code($responseCode);
        } else {
            http_response_code(200);
        }
    }else{
        $headers = apache_request_headers();
        if (isset($headers['Authorization'])) {

            $user = new User(null, null);
            $user->setUserName($array->userName);
            $user->setPassword($array->password);
            $user->setRole($array->role);
            $dataArray = $user->save();


            //delvolvemos una respuesta
            header('Content-Type: application/json');
            if (isset($dataArray["result"]["error_id"])) {
                $responseCode = $dataArray["result"]["error_id"];
                http_response_code($responseCode);
            } else {
                http_response_code(200);
            }
        }else{
            $response = array (
                "status" => "Error",
                "Message" => "No tienes acesso"
            );
        }
    }
    echo json_encode($dataArray);


}elseif ($_SERVER['REQUEST_METHOD'] == "DELETE") {


    $headers = apache_request_headers();
    if (isset($headers['Authorization'])) {
        $postBody = file_get_contents("php://input");
        $postBody = json_decode($postBody);
        $dataArray = User::delete($postBody->id);
        header('Content-Type: application/json');
        if (isset($dataArray["result"]["error_id"])) {
            $responseCode = $dataArray["result"]["error_id"];
            http_response_code($responseCode);
        } else {
            http_response_code(200);
        }
        echo json_encode($dataArray);
    } else {
        $response = array(
            "status" => "Error",
            "Message" => "No tienes acesso"
        );
    }
}else{
    header('Content-Type: application/json');
    $responses = new Responses();
    $dataArray = $responses->error_405();
    echo json_encode($dataArray);

}


