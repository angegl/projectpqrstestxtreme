<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Project PQRS Test Xtreme</title>
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
        <script src="https://kit.fontawesome.com/a59a2b590c.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
        <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
        <script type="text/javascript" src="assets/js/jquery-3.5.0.min.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
        <script src="//cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    </head>
    <body>
    <?php
        require_once("./view/getMenu.php");
    ?>
    </body>
</html>