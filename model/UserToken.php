<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author USER
 */
class UserToken {
    var $id;
    var $idUser;
    var $Token;
    var $status;
    var $date;
    
    function __construct($field, $value) {
        if ($field!=null) {
            if (is_array($field)) {
                foreach ($field as $Variable => $Value) $this->$Variable=$Value;
                $this->loadAttributesUppercase($field);
            } else {
                $query="select id, idUser , token, status, date from usersToken where $field='$value'";
                $result = Connector::executeQuery($query, null);
                if(is_array($result)){
                    if (count($result)>0){
                        foreach ($result[0] as $Variable => $Value) $this->$Variable=$Value;
                        $this->loadAttributesUppercase($result[0]);
                    }
                }
            }
        }
    }
    
    private function loadAttributesUppercase($array) {
        $this->idUser = $array['idUser'];
    }

    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }
    

    function getIdUser() {
        return $this->IdUser;
    }

    function setIdUser($IdUser) {
        $this->IdUser = $IdUser;
    }
    

    function getToken() {
        return $this->token;
    }

    function setToken($token) {
        $this->token = $token;
    }

    function getStatus() {
        return $this->status;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function getDate() {
        return $this->date;
    }

    function setDate($date) {
        $this->date = $date;
    }

    public static function insertToken($idUser){
        $val = true;
        $token = bin2hex(openssl_random_pseudo_bytes(16,$val));
        $date = date("Y-m-d H:i");
        $status = "a";
        $query = "INSERT INTO usersToken (idUser,token, status,date)VALUES('$idUser','$token','$status','$date')";
        $check = Connector::executeQuery($query, null);
        if($check){
            return $token;
        }else{
            return false;
        }
    }

    public static function updateToken($tokenId){
        $val = true;
        $token = bin2hex(openssl_random_pseudo_bytes(16,$val));
        $date = date("Y-m-d H:i");
        $query = "UPDATE usersToken SET token='$token', date = '$date' WHERE id = '$tokenId' ";
        $check = Connector::executeQuery($query, null);
        if($check){
            return $token;
        }else{
            return 0;
        }
    }


    
        
}
