<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author USER
 */
class User {
    var $id;
    var $userName;
    var $password;
    var $role;
    
    function __construct($field, $value) {
        if ($field!=null) {
            if (is_array($field)) {
                foreach ($field as $Variable => $Value) $this->$Variable=$Value;
                $this->loadAttributesUppercase($field);
            } else {
                $query="select id, userName, password, role from users where $field='$value'";
                $result = Connector::executeQuery($query, null);
                if(is_array($result)){
                    if (count($result)>0){
                        foreach ($result[0] as $Variable => $Value) $this->$Variable=$Value;
                        $this->loadAttributesUppercase($result[0]);
                    }
                }
            }
        }
    }
    
    private function loadAttributesUppercase($array) {
        $this->userName = $array['userName'];
    }
    
    function getId() {
        return $this->id;
    }

    function getUserName() {
        return $this->userName;
    }

    function getPassword() {
        return $this->password;
    }

    function getRole() {
        return $this->role;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setUserName($userName) {
        $this->userName = $userName;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setRole($role) {
        $this->role = $role;
    }

    public function save(){
        $password = md5($this->password);
        $query="insert into users ( userName, password, role) values ('$this->userName','$password', '$this->role');";
        if(Connector::executeQuery($query,null)){
            $response = array(
                "status" => "ok",
                "Message" => "Datos almacenados correctamente"
            );
        }else {
            $response = array(
                "status" => "Error",
                "Message" => "Error al almacenar los datos"
            );

        }
        return $response;
    }

    public function delete($id){
        $query="delete from users where id = $id;";
        if(Connector::executeQuery($query,null)){
            $response = array(
                "status" => "ok",
                "Message" => "Datos eliminados correctamente"
            );
        }else {
            $response = array(
                "status" => "Error",
                "Message" => "Error al eliminar los datos"
            );

        }
        return $response;
    }

    public static function getList($filter){
        if ($filter!=null) $filter=" where $filter";
        $query="select id, userName, password, role from users $filter";
        return Connector::executeQuery($query,null);;
    }
    
    public static function getListInObjects($filter){
        $data= User::getList($filter);
        $users= Array();
        if(is_array($data)){
            for ($i = 0; $i < count($data); $i++) {
                $users[$i]=new User($data[$i], null);
            }
        }
        return $users;
    }

    

    public function login($json){
      
        $responses = new Responses;
        $data = json_decode($json,true);
        if(!isset($data['userName']) || !isset($data["password"])){
            //error con los campos
            return $responses->error_400();
        }else{
            //todo esta bien 
            $user = $data['userName'];
            $password = $data['password'];
            $password = md5($password);
            $data = User::getListInObjects(" userName = '$user'");
            if($data){
                //verificar si la contraseña es igual
                    if($password == $data[0]->password){
                            //crear el token
                            
                            $userToken = new UserToken('idUser' , $data[0]->id);
                            $userToken = (array)$userToken;
                            if(isset($userToken['id'])){
                                $check  = UserToken::updateToken($userToken['id']);
                            }else {
                                $check  = UserToken::insertToken($data[0]->id);
                            }
                            if($check){
                                    // si se guardo
                                    $result = $responses->response;
                                    $result["result"] = array(
                                        "token" => $check,
                                        "idUser" => $data[0]->id,
                                        "role" => $data[0]->role
                                    );
                                    
                                    return $result;
                            }else{
                                    //error al guardar
                                    return $responses->error_500("Error interno, No hemos podido guardar");
                            }
                    }else{
                        //la contraseña no es igual
                        return $responses->error_200("El password es invalido");
                    }
            }else{
                //no existe el usuario
                return $responses->error_200("El usuario $user  no existe ");
            }
        }
    }
    
}
