<?php


class Pqr
{
    var $id;
    var $idUser;
    var $typePqr;
    var $affair;
    var $status;
    var $creationDate;
    var $deadline;

    function __construct($field, $value) {
        if ($field!=null) {
            if (is_array($field)) {
                foreach ($field as $Variable => $Value) $this->$Variable=$Value;
                $this->loadAttributesUppercase($field);
            } else {
                $query="select id ,idUser ,typePqr ,affair ,status ,creationDate ,deadline from pqrs where $field='$value'";
                $result = Connector::executeQuery($query, null);
                if(is_array($result)){
                    if (count($result)>0){
                        foreach ($result[0] as $Variable => $Value) $this->$Variable=$Value;
                        $this->loadAttributesUppercase($result[0]);
                    }
                }
            }
        }
    }

    private function loadAttributesUppercase($array) {
        $this->idUser = $array['idUser'];
        $this->typePqr = $array['typePqr'];
        $this->creationDate = $array['creationDate'];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->idUser;
    }


    /**
     * @param mixed $idUser
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    }

    /**
     * @return mixed
     */
    public function getTypePqr()
    {
        return $this->typePqr;
    }

    /**
     * @param mixed $typePqr
     */
    public function setTypePqr($typePqr)
    {
        $this->typePqr = $typePqr;
    }

    /**
     * @return mixed
     */
    public function getAffair()
    {
        return $this->affair;
    }

    /**
     * @param mixed $affair
     */
    public function setAffair($affair)
    {
        $this->affair = $affair;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param mixed $creationDate
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    }

    /**
     * @return mixed
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * @param mixed $deadline
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;
    }

    public  function create($idUser, $typePqr, $affair, $status){
        $creationDate = date("Y-m-d");
        $check= true;
        if($typePqr=="P") $days = 7;
        elseif($typePqr=="Q") $days = 3;
        elseif($typePqr=="R") $days = 2;
        else $check = false;
        if($check){
            $deadline = date("Y-m-d",strtotime($creationDate."+ $days days"));
            $query="insert into pqrs (idUser ,typePqr ,affair ,status ,creationDate ,deadline) values ('$idUser','$typePqr', '$affair', '$status', '$creationDate', '$deadline');";
            if(Connector::executeQuery($query,null)){
                $response = array(
                    "status" => "ok",
                    "Message" => "Datos almacenados correctamente"
                );
            }else $response = array(
                "status" => "Error",
                "Message" => "Error al almacenar los datos"
            );
            return $response;
        }else {
            $response = array(
                "status" => "Error",
                "Message" => "Tipo de pqr no soportado"
            );
            return $response;
        }
    }

    public function update(){
        $creationDate = date("Y-m-d H:i");
        $query="update pqrs set set typePqr = '$this->typePqr', affair = '$this->affair', status = '$this->status',creationDate = '$creationDate',deadline = '$this->deadline' where id = $this->id; ";
        if(Connector::executeQuery($query,null)){
            $response = array(
                "status" => "ok",
                "Message" => "Datos almacenados correctamente"
            );
        }else {
            $response = array(
                "status" => "Error",
                "Message" => "Error al almacenar los datos"
            );

        }
        return $response;
    }
    public function delete($id){
        $query="delete from pqrs where id= $id;";
        if(Connector::executeQuery($query,null)){
            $response = array(
                "status" => "ok",
                "Message" => "Datos eliminados correctamente"
            );
        }else {
            $response = array(
                "status" => "Error",
                "Message" => "Error al eliminar los datos"
            );

        }
        return $response;
    }

    public function getList($filter){
        if ($filter!=null) $filter=" and $filter";
        $query="select pqrs.id ,idUser, userName ,typePqr ,affair ,status ,creationDate ,deadline from pqrs, users where users.id = idUser  $filter";
        return Connector::executeQuery($query,null);;
    }

    public static function getListInObjects($filter){
        $data= Pqr::getList($filter);
        $pqrs= Array();
        if(is_array($data)){
            for ($i = 0; $i < count($data); $i++) {
                $pqrs[$i]=new Pqr($data[$i], null);
            }
        }
        return $pqrs;
    }


}